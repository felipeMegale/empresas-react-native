![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys
Este documento `README.md` tem como objetivo fornecer as informações a respeito do meu fork do projeto Empresas.

---

### Executando a aplicação ###
Usei um simulador de iPhone 8 (iOS 13.0) no macOS e um Moto G5S Plus para rodar e testar a aplicação

* ``` git clone https://felipeMegale@bitbucket.org/felipeMegale/empresas-react-native.git ```
* ``` cd empresas-react-native/Empresas```
* ``` yarn install ```
  * Se rodar no simulador iOS, ``` cd ios && pod install && cd ..```
* ``` react-native run-ios ``` ou ``` react-native run-android ```

### Bibliotecas Utilizadas
* react-navigation e suas dependências (abaixo)
  * react-native-gesture-handler
  * react-native-reanimated
  * react-native-screens
  * react-navigation-stack

Usei apenas esta lib para poder ter um esquema de roteamento entre componentes e paginação no meu app!
Os requests foram feitos usando a API fetch e e tratadas com as boas e velhas Promises.

### Ponto em que parei
* Implementado: 
  * Login
  * Listagem de empresas: ``` /enterprises ```
  * Detalhamento de empresas: ``` /enterprises/{id} ```

* Não implementado:
  * Filtro de Empresas por nome e tipo: ``` /enterprises?enterprise_types={type}&name={name} ```

### Arrependimento
* Não ter usado redux. Passar os custom headers por props não é legal
