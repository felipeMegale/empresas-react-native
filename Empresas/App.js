import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Login from './components/Login';
import Main from './components/Main';
import AllCompanies from './components/AllCompanies';
import Search from './components/Search';

console.disableYellowBox = true;

const App = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      header: null,
    }
  },
  Main: {
    screen: Main,
    navigationOptions: {
      title: 'Welcome to Companies',
    }
  },
  AllCompanies: {
    screen: AllCompanies,
    navigationOptions: {
      title: 'All Companies',
    }
  },
  Search: {
    screen: Search,
    navigationOptions: {
      title: 'Search',
    }
  }
});

export default createAppContainer(App);
