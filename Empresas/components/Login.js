import React, { Fragment, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ImageBackground,
  Button,
  KeyboardAvoidingView,
  Dimensions,
  Alert,
} from 'react-native';

import { withNavigation } from 'react-navigation';

const submitBtnPressHandler = (props, auth) => {
  let loginUrl = 'http://empresas.ioasys.com.br/api/v1/users/auth/sign_in';
  let requestHeaders = {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(auth),
  };

  if (!auth.email || !auth.password) {
    Alert.alert(
      "Error",
      "Invalid username or password",
      [
        {
          text: 'OK',
          onPress: () => { },
        }
      ]
    );
  } else {
    fetch(loginUrl, requestHeaders).then(response => {
      const { "access-token": accessToken, "client": client, "uid": uid } = response.headers.map;
      const customHeaders = {
        accessToken: accessToken,
        client: client,
        uid: uid
      };
      response.json().then(responseJson => {
        if (!responseJson.success) {
          Alert.alert(
            "Error",
            responseJson.errors.join('\n'),
            [
              {
                text: 'OK',
              }
            ]
          );
        } else {
          props.navigation.navigate("Main", { customHeaders: customHeaders, myCompany: responseJson });
        }
      });
    });
  }

}

const Login = (props) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  return (
    <Fragment>
      <View>
        <KeyboardAvoidingView>
          <ImageBackground source={require('../assets/login-bg.jpg')} style={styles.loginBg}>
            <View style={styles.loginCard}>
              <Text style={styles.loginTitle}>Login to Companies</Text>
              <View style={styles.loginField}>
                <TextInput placeholder={"Username / e-mail"}
                  placeholderTextColor={"#444"}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  style={styles.loginTxtInput}
                  value={email}
                  onChangeText={(email) => setEmail(email)} />
              </View>
              <View style={styles.loginField}>
                <TextInput placeholder={"Password"}
                  placeholderTextColor={"#444"}
                  secureTextEntry={true}
                  style={styles.loginTxtInput}
                  value={password}
                  onChangeText={(password) => setPassword(password)} />
              </View>
              <View style={styles.loginBtn}>
                <Button title={"Submit"}
                  color="black"
                  onPress={() => { submitBtnPressHandler(props, { email: email, password: password }) }} />
              </View>
            </View>
          </ImageBackground>
        </KeyboardAvoidingView>
      </View>
    </Fragment>
  )
}

const styles = StyleSheet.create({
  loginBg: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  loginTitle: {
    fontSize: 20
  },
  loginCard: {
    backgroundColor: 'gray',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    maxWidth: '70%',
    maxHeight: '50%',
    opacity: 0.98,
    shadowOpacity: 0.40,
    borderRadius: 5,
    marginHorizontal: '15%',
    marginTop: '25%',
    paddingHorizontal: 10
  },
  loginField: {
    borderColor: 'black',
    borderRadius: 5,
    borderWidth: 1,
    height: 45,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#ddd'
  },
  loginTxtInput: {
    fontSize: 20,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  loginBtn: {
    borderColor: 'black',
    borderRadius: 5,
    borderWidth: 1,
  }
});

export default withNavigation(Login);