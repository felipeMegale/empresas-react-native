import React from 'react';
import {
  StyleSheet,
  FlatList,
  SafeAreaView
} from 'react-native';

import { withNavigation } from 'react-navigation';

import Company from './Company';

const AllCompanies = (props) => {
  const { navigation } = props;
  const { enterprises } = navigation.getParam('companies');
  const { method, headers } = navigation.getParam('requestHeaders');
  const customHeaders = {
    method: method,
    headers: headers,
  };

  return (
    <SafeAreaView>
      <FlatList
        data={enterprises}
        renderItem={({ item }) => {
          return (
            <Company customHeaders={customHeaders} id={item.id} img={item.photo} title={item.enterprise_name} />
          );
        }}
        keyExtractor={item => item.id}
        style={styles.flatList} />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  flatList: {
    padding: 5,
  },
});

export default withNavigation(AllCompanies);