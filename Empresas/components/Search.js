import React from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
} from 'react-native';
import { withNavigation } from 'react-navigation';

const Search = (props) => {
  const { navigation } = props;
  const requestHeaders = navigation.getParam('requestHeaders');

  return (
    <SafeAreaView>
      <Text style={styles.searchText}>You should've been able to search here</Text>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  searchText: {
    textAlign: 'justify'
  }
});

export default withNavigation(Search);