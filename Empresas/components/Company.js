import React, { Fragment, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Image,
  Alert,
  Modal,
  SafeAreaView,
  Button
} from 'react-native';

const photoBaseURL = "http://empresas.ioasys.com.br";
const apiBaseURL = "http://empresas.ioasys.com.br/api/v1/enterprises/";

const Company = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [modalInfo, setModalInfo] = useState({});

  const companyItemPressHandler = (id, requestHeaders) => {
    const url = `${apiBaseURL}${id}`;
    fetch(url, requestHeaders).then(response => {
      response.json().then(responseJson => {
        let { enterprise } = responseJson;
        let { description, city, country, value, shares, share_price, enterprise_type } = enterprise;
        let companyInfo = {
          description: description,
          city: city,
          country: country,
          value: value,
          shares: shares,
          share_price: share_price,
          enterprise_type: enterprise_type.enterprise_type_name,
        }
        setModalInfo(companyInfo);
        setModalVisible(true);
      })
    }).catch(error => Alert.alert("Error", error, [{ text: 'OK' }]));
  }

  return (
    <Fragment>
      <SafeAreaView>
        <Modal
          animationType="slide"
          transparent={false}
          visible={modalVisible} >
          <View style={styles.modalView}>
            <View style={styles.modalHeader}>
              <Image
                source={props.img ? { uri: `${photoBaseURL}${props.img}` } : require('../assets/unknown.jpg')}
                style={styles.companyImage}
              ></Image>
              <Text style={styles.companyName}>{props.title}</Text>
            </View>
            <View>
              <Text style={{ textAlign: 'justify' }}>{modalInfo.description}</Text>
              <View style={styles.modalInfoDetails}>
                <Text>City: {modalInfo.city}</Text>
                <Text>Country: {modalInfo.country}</Text>
                <Text>Enterprise type: {modalInfo.enterprise_type}</Text>
                <Text>Value: US$ {modalVisible ? modalInfo.value.toFixed(2) : modalInfo.value}</Text>
                <Text>Shares available: {modalInfo.shares}</Text>
                <Text>Share price: US$ {modalVisible ? modalInfo.share_price.toFixed(2) : modalInfo.share_price}</Text>
              </View>
            </View>
            <Button title="Close" onPress={() => setModalVisible(!modalVisible)} />
          </View>
        </Modal>
      </SafeAreaView>

      <TouchableHighlight underlayColor='gray' onPress={() => companyItemPressHandler(props.id, props.customHeaders)}>
        <View style={styles.companyView}>
          <Image
            source={props.img ? { uri: `${photoBaseURL}${props.img}` } : require('../assets/unknown.jpg')}
            style={styles.companyImage}
          ></Image>
          <Text style={styles.companyName}>{props.title}</Text>
        </View>
      </TouchableHighlight>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  companyView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderRadius: 5,
    borderColor: 'black',
    borderWidth: 1,
    marginTop: 7,
  },
  companyImage: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
  },
  companyName: {
    fontSize: 20,
    fontWeight: 'bold',
    alignContent: 'center',
    paddingHorizontal: 5,
  },
  modalView: {
    padding: 30,
  },
  modalHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    paddingBottom: 5,
    marginBottom: 5,
  },
  modalFooter: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },
  modalInfoDetails: {
    paddingTop: 5,
  },

});

export default Company;