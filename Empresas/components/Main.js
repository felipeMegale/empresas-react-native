import React, { Fragment } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Button,
  ScrollView
} from 'react-native';

import { withNavigation } from 'react-navigation';

const companiesBtnPressHandler = (props, headers) => {
  const url = 'http://empresas.ioasys.com.br/api/v1/enterprises';
  const requestHeaders = {
    method: 'get',
    headers: {
      'Content-type': 'application/json',
      'access-token': headers.accessToken,
      'client': headers.client,
      'uid': headers.uid
    }
  }

  fetch(url, requestHeaders).then(response => {
    response.json().then(responseJson => {
      props.navigation.navigate("AllCompanies", { requestHeaders: requestHeaders, companies: responseJson });
    });
  });
}

const searchBtnPressHandler = (props, headers) => {
  const requestHeaders = {
    method: 'get',
    headers: {
      'Content-type': 'application/json',
      'access-token': headers.accessToken,
      'client': headers.client,
      'uid': headers.uid
    }
  }
  props.navigation.navigate("Search", { requestHeaders: requestHeaders });
}

const Main = (props) => {
  const { navigation } = props;

  const customHeaders = navigation.getParam('customHeaders');
  const myCompany = navigation.getParam('myCompany');

  const { investor_name, email, city, country, balance,
    photo, portfolio_value, super_angel } = myCompany.investor;

  return (
    <Fragment>
      <ScrollView>
        <View style={styles.investorHeader}>
          <Image source={photo ? photo : require('../assets/unknown.jpg')}
            style={{
              height: 100,
              width: 100,
              borderRadius: 100 / 2
            }} />
          <Text style={styles.investorName}>{investor_name}</Text>
        </View>
        <View>
          <Text style={styles.investorInfoLabel}>Balance:</Text>
          <Text style={{ ...styles.investorInfoValue, color: '#85bb65' }}>US$ {balance.toFixed(2)}</Text>
          <Text style={styles.investorInfoLabel}>Portfolio Value:</Text>
          <Text style={{ ...styles.investorInfoValue, color: '#85bb65' }}>US$ {portfolio_value.toFixed(2)}</Text>
          <Text style={styles.investorInfoLabel}>Super Angel:</Text>
          <Text style={styles.investorInfoValue}>{super_angel ? 'Yes!' : 'No :('}</Text>
          <Text style={styles.investorInfoLabel}>EMail:</Text>
          <Text style={styles.investorInfoValue}>{email}</Text>
          <Text style={styles.investorInfoLabel}>City:</Text>
          <Text style={styles.investorInfoValue}>{city}</Text>
          <Text style={styles.investorInfoLabel}>Country:</Text>
          <Text style={styles.investorInfoValue}>{country}</Text>
        </View>
      </ScrollView>
      <View style={styles.footer}>
        <Button title="All Companies" onPress={() => { companiesBtnPressHandler(props, customHeaders) }} />
        <Button title="Search" onPress={() => { searchBtnPressHandler(props, customHeaders) }} />
      </View>
    </Fragment>
  )
}

const styles = StyleSheet.create({
  investorHeader: {
    flexDirection: 'column',
    alignContent: 'center',
    alignItems: 'center'
  },
  investorName: {
    fontSize: 20,
    padding: 15,
  },
  investorInfoLabel: {
    paddingLeft: 25,
    fontSize: 18,
  },
  investorInfoValue: {
    paddingLeft: 35,
    fontSize: 14,
    marginBottom: 5,
  },
  footer: {
    flexDirection: 'row',
    position: 'relative',
    bottom: 0,
    paddingHorizontal: 25,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'space-between'
  }
});

export default withNavigation(Main);